﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evernote
{

    class TreeData
    {
        public TreeData()
        {
            this.Tree = this.CreateTree();
        }
        public TreeViewMode Tree { get; set; }
        public TreeViewMode Shortcuts { get; set; }

        TreeViewMode CreateTree()
        {
            var tree = new TreeViewMode();
            this.Shortcuts = this.CreateShortCuts();
            tree.Children.Add(this.Shortcuts);
            return tree;
        }
        TreeViewMode CreateShortCuts()
        {
            var shortcuts = new TreeViewMode()
            {
                Image = @"WelResources\Images\tool\pix24\tool_60.png",
                Text = "Shortcuts",
                ContentKey1 = "ShortcutsContentControl",
                IsExpanded = true,
            };

            var bob = new TreeViewMode()
            {
                Image = @"WelResources\Images\face\pix24\boy_00.png",
                Text = "Bob",
                Text2 = "82",
                ContentKey1 = "ArticleListControl",
            };
            bob.Children.Add(new TreeViewMode()
            {
                Image = @"WelResources\Images\face\pix24\boy_01.png",
                Text = "Eric",
                Text2 = "30",
                ContentKey1 = "ArticleListControl",
            });

            bob.Children.Add(new TreeViewMode()
            {
                Image = @"WelResources\Images\face\pix24\boy_02.png",
                Text = "Ellen",
                Text2 = "111",
                ContentKey1 = "ArticleListControl",
            });

            shortcuts.Children.Add(bob);

            shortcuts.Children.Add(new TreeViewMode()
            {
                Image = @"WelResources\Images\face\pix24\boy_01.png",
                Text = "Eric",
                Text2 = "30",
                ContentKey1 = "ArticleListControl",
            });

            shortcuts.Children.Add(new TreeViewMode()
            {
                Image = @"WelResources\Images\face\pix24\boy_02.png",
                Text = "Ellen",
                Text2 = "111",
                ContentKey1 = "ArticleListControl",
            });
            //shortcuts.Children.IsExpanded = true;
            return shortcuts;
        }
    }

}
