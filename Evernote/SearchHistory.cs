﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evernote
{
    public class SearchHistory : List<string>
    {
        public SearchHistory()
        {
            foreach (string item in this.GetValues())
            {
                this.Add(item);
            }
        }

        IEnumerable<string> GetValues()
        {
            yield return "Abu-Hadba";
            yield return "Allard";
            yield return "Holse Andersen";
            yield return "Anderson";
            yield return "Arbogast";
            yield return "Ayala";
            yield return "Bach";
            yield return "Béjar";
            yield return "Belfiore";
            yield return "Bevington";
            yield return "Brod";
            yield return "Brooks";
            yield return "Brummel";
            yield return "Burt";
            yield return "Capossela";
            yield return "Charney";
            yield return "Chrapaty";
            yield return "Courtois";
            yield return "Crozier";
            yield return "DelBene";
            yield return "Delman";
            yield return "DeVaan";
            yield return "Elliott";
            yield return "Elop";
            yield return "Fathi";
            yield return "George";
            yield return "Gibbons";
            yield return "Golden";
            yield return "Gounares";
            yield return "Guggenheimer";
            yield return "Gupta";
            yield return "Hey";
            yield return "Higuchi";
            yield return "Ho";
            yield return "Hogan";
            yield return "Holland";
            yield return "Holmdahl";
            yield return "Huston";
            yield return "Jha";
            yield return "Jones";
            yield return "Jorgensen";
            yield return "Kaplan";
            yield return "Kelly";
            yield return "Khaki";
            yield return "Kim";
            yield return "Klein";
            yield return "Koch";
            yield return "Kummert";
            yield return "Larson-Green";
            yield return "Leblond";
            yield return "Lees";
            yield return "Lervik";
            yield return "Levin";
            yield return "Lewin";
            yield return "Lichtman";
            yield return "Liddell";
            yield return "Liffick";
            yield return "MacDonald";
            yield return "Markezich";
            yield return "Martinez";
            yield return "Mathews";
            yield return "Mattrick";
            yield return "Matz";
            yield return "McAndrews";
            yield return "McAniff";
            yield return "Mehdi";
            yield return "Minervino";
            yield return "Mitchell";
            yield return "Moberg";
            yield return "Mount";
            yield return "Muglia";
            yield return "Mundie";
            yield return "Myerson";
            yield return "Nadella";
            yield return "Nash";
            yield return "Neupert";
            yield return "Ozzie";
            yield return "Singh Pall";
            yield return "Park";
            yield return "Paolucci";
            yield return "Parthasarathy";
            yield return "Passman";
            yield return "Peracca";
            yield return "Peters";
            yield return "Peterson";
            yield return "Phelps";
            yield return "Poole";
            yield return "Rashid";
            yield return "Reller";
            yield return "Ritchie";
            yield return "Rodriguez";
            yield return "Rosini";
            yield return "Roskill";
            yield return "Rudder";
            yield return "Schappert";
            yield return "Scott";
            yield return "Sheldon";
            yield return "Shum";
            yield return "Sinofsky";
            yield return "Smith";
            yield return "Snapp";
            yield return "Srivastava";
            yield return "Tatarinov";
            yield return "Teper";
            yield return "Thompson";
            yield return "Thompson";
            yield return "Tobey";
            yield return "Treadwell";
            yield return "Turner";
            yield return "Vaskevitch";
            yield return "Veghte";
            yield return "Vigil";
            yield return "Wahbe";
            yield return "Warren";
            yield return "Watson";
            yield return "Westlake";
            yield return "Witts";
            yield return "Youngjohns";
            yield return "Zhang";
            yield return "Zinn";

        }
    }
}
