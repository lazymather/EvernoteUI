﻿using System.Collections;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;

namespace Nichicom.Wel
{
    /// <summary>
    /// リソース管理のクラス
    /// </summary>
    public class ResourceMan
    {
        private static Assembly s_objResourceAssembly;
        private static readonly string s_strImageRoot = "Nichicom.Wel.Forms.WelResources.Images";

        /// <summary>
        /// staticコンストラクタです。
        /// </summary>
        static ResourceMan()
        {
            s_objResourceAssembly = Assembly.GetExecutingAssembly();
        }
        
        /// <summary>
        /// 画像を取得します。
        /// </summary>
        /// <param name="imageName">画像名</param>
        /// <returns>画像</returns>
        public static Image GetImage(string imageName)
        {
            string strResourceName = s_strImageRoot + "." + imageName;
            Stream objStream = s_objResourceAssembly.GetManifestResourceStream(strResourceName);

            if (objStream == null)
            {
                return null;
            }
            Image image = new Image();
            //image.Source=  ImageSource
            //return Image.FromStream(objStream);
            return null;
        }
        /// <summary>
        /// 印刷用画像を取得します。
        /// </summary>
        /// <param name="pixel">ピクセル</param>
        /// <param name="iconno">アイコン番号</param>
        /// <returns>画像</returns>
        public static Image GetPrintImage(ImagePixel pixel, int iconno)
        {
            return GetPrintImage(pixel, (long)iconno);
        }
        /// <summary>
        /// 印刷用画像を取得します。
        /// </summary>
        /// <param name="pixel">ピクセル</param>
        /// <param name="iconno">アイコン番号</param>
        /// <returns>画像</returns>
        public static Image GetPrintImage(ImagePixel pixel, long iconno)
        {
            return GetImage(GetPrintImageName(pixel, iconno));
        }
        /// <summary>
        /// 印刷用画像名称を取得します。
        /// </summary>
        /// <param name="pixel">ピクセル</param>
        /// <param name="iconno">アイコン番号</param>
        /// <returns>イメージ名称</returns>
        public static string GetPrintImageName(ImagePixel pixel, int iconno)
        {
            return GetPrintImageName(pixel, (long)iconno);
        }
        /// <summary>
        /// 印刷用画像名称を取得します。
        /// </summary>
        /// <param name="pixel">ピクセル</param>
        /// <param name="iconno">アイコン番号</param>
        /// <returns>イメージ名称</returns>
        public static string GetPrintImageName(ImagePixel pixel, long iconno)
        {
            return string.Format("print.pix{0}.prt_{1:00}.png"
                , (int)pixel
                , iconno);
        }
        /// <summary>
        /// 同じディレクトリにいる画像を取得します。
        /// </summary>
        /// <param name="prefixOfPath">AegisResources.Images.icon.pix16のように</param>
        /// <returns>画像の配列</returns>
        public static ArrayList GetFullPathInResources(string prefixOfPath)
        {
            prefixOfPath = s_strImageRoot + "." + prefixOfPath;
            string[] strNames = s_objResourceAssembly.GetManifestResourceNames();
            ArrayList lstNames = new ArrayList();
            foreach (string name in strNames)
            {
                if (name.StartsWith(prefixOfPath))
                {
                    lstNames.Add(name);
                }
            }
            return lstNames;
        }

        /// <summary>
        /// フルパスで画像を取得します。
        /// </summary>
        /// <param name="fullPaths">AegisResources.Images.icon.pix16.btn_82.pngのように</param>
        /// <returns>画像の配列</returns>
        public static Image[] GetImages(ArrayList fullPaths)
        {
            Image[] imgs = new Image[fullPaths.Count];
            Stream objStream;
            for (int i = 0; i < imgs.Length; i++)
            {
                objStream = s_objResourceAssembly.GetManifestResourceStream(fullPaths[i].ToString());
                if (null != objStream)
                {
                    imgs[i] = null;// Image.FromStream(objStream);
                }
            }
            return imgs;
        }
        /// <summary>
        /// 画像ピクセルサイズ
        /// </summary>
        public enum ImagePixel : int
        {
            /// <summary>
            /// 16ピクセル
            /// </summary>
            Pix16 =16,
            /// <summary>
            /// 24ピクセル
            /// </summary>
            Pix24 = 24,
            /// <summary>
            /// 32ピクセル
            /// </summary>
            Pix32 = 32,
            /// <summary>
            /// 48ピクセル
            /// </summary>
            Pix48 = 48,
            /// <summary>
            /// 64ピクセル
            /// </summary>
            Pix64 = 64,
        }
    }
}
