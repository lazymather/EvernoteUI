﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evernote
{
    public interface IContextSelectionService
    {
        event EventHandler SelectionChanged;
        void SetSelectedContext(object context);
        object PrimarySelection { get; }
    }
}
